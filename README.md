********************************************************************************
                                                                                
                                                          :::      ::::::::     
     README.md                                          :+:      :+:    :+:     
                                                      +:+ +:+         +:+       
     By: apakhomo <marvin@42.fr>                    +#+  +:+       +#+          
                                                  +#+#+#+#+#+   +#+             
     Created: 2017/12/29 12:13:53 by apakhomo          #+#    #+#               
     Updated: 2017/12/29 12:20:50 by apakhomo         ###   ########.fr         
                                                                                
********************************************************************************

Use:

git clone https://gitlab.com/omentes/ft_fillit_checker.git

cd ft_fillit_checker

cp -R ./testsrcs  ~/YOUR_FILLIT_DIR/ && cp test_fillit.sh ~/YOUR_FILLIT_DIR/

cd ~/YOUR_FILLIT_DIR/

bash test_fillit.sh